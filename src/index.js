const express = require('express');
const app = express();
const HTTP = 'http://127.0.0.1:3001';
const urlDB = "mongodb://127.0.0.1:27017";
const nameCollection = 'link';
const baseName = "usersdb";
const mongoClient = require("mongodb").MongoClient;
let db;
mongoClient.connect(urlDB).then(client => {
    db = client.db(baseName)
});
const bodyParser = require('body-parser');
app.use(bodyParser.json());
let deleteLink = async (_id, delay) => {
    const collection = db.collection(nameCollection);
    let obj = await collection.findOne({_id});
    collection.deleteOne(obj, {expireAfterSeconds: delay})
};
let convertStrDelay = (delay) => {
    let arr = delay.split('');
    let objDelay = {d: [], h: [], m: [], s: [], index: []};
    for (let i = 0; i < arr.length; i++) {
        if (isNaN(arr[i])) objDelay.index.push(i);
    }
    for (let i = 0; i < objDelay.index.length; i++) {
        let index = objDelay.index[i];
        let prevIndex = objDelay.index[i - 1];
        let j = index;
        if (i === 0) prevIndex = 0;
        for (; j >= prevIndex; j--) {
            if (!isNaN(arr[j])) {
                objDelay[arr[index]].push(arr[j]);
            }
        }
    }
    return getMilliseconds(objDelay);
};
let getMilliseconds = (objDelay) => {
    let delay = 0;
    for (let key in objDelay) {
        if (objDelay[key].length === 0 || key === 'index') continue;
        let number = +objDelay[key].reverse().join('');
        if (key === 'd') delay += (number * 86400000);
        if (key === 'h') delay += (number * 3600000);
        if (key === 'm') delay += (number * 60000);
        if (key === 's') delay += (number * 1000);
    }
    return delay
};

app.post('/link', (req, res) => {
    console.log(req.body);
    let link = req.body;
    let randomStr = (new Date % 9e6).toString(36);
    link.shortLink = HTTP + `/link/${randomStr}`;
    link.str = randomStr;
    const collection = db.collection(nameCollection);
    collection.insertOne(link, () => {
        let delay = 6000;
        if (link.ttl) delay = convertStrDelay(link.ttl);
        deleteLink(link.str, delay);
    });
    return res.status(200).json({shortLink: link.shortLink});
});
app.get("/link/:shortLink", async (req, res) => {
    const collection = db.collection(nameCollection);
    let x = await collection.findOne({str: req.params.shortLink});
    res.redirect(x.link)
});

app.listen(3001);
